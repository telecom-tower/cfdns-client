// Copyright 2017 Jacques Supcik, HEIA-FR
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Telecom Tower DynDns (cloudflare) client - Updater
//

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

type CloudFlareMessage struct {
	Result   interface{}   `json:"result"`
	Success  bool          `json:"success"`
	Errors   []interface{} `json:"errors"`
	Messages []interface{} `json:"messages"`
}

var params struct {
	interfaceName string
	authEmail     string
	authKey       string
	domain        string
	hostname      string
	refresh       int
}

func newRequest(method string, path string, body io.Reader) (*http.Request, error) {
	u := url.URL{
		Scheme: "https",
		Host:   "api.cloudflare.com",
		Path:   path,
	}
	req, err := http.NewRequest(method, u.String(), body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-Auth-Email", params.authEmail)
	req.Header.Set("X-Auth-Key", params.authKey)
	req.Header.Set("Content-Type", "application/json")
	return req, nil
}

func addParam(req *http.Request, key string, val string) {
	v := req.URL.Query()
	v.Set(key, val)
	req.URL.RawQuery = v.Encode()
}

func getZoneID(zone string) (string, error) {
	req, err := newRequest("GET", "client/v4/zones", nil)
	if err != nil {
		return "", err
	}
	addParam(req, "name", zone)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	payload := new(CloudFlareMessage)
	err = json.NewDecoder(resp.Body).Decode(&payload)
	if err != nil {
		return "", err
	}
	if !payload.Success {
		return "", errors.New(fmt.Sprintf("%v", payload.Errors))
	}

	element, ok := payload.Result.([]interface{})
	if !ok {
		return "", errors.New("result is not an array")
	}

	if len(element) != 1 {
		return "", errors.New("result has more than one element")
	}

	item, ok := element[0].(map[string]interface{})
	if !ok {
		return "", errors.New("result[0] is not a map")
	}

	res, ok := item["id"]
	if !ok {
		return "", errors.New("result[0] has no id")
	}

	return res.(string), nil
}

func getRecord(zoneId string, name string) (map[string]interface{}, error) {

	req, err := newRequest("GET", fmt.Sprintf("client/v4/zones/%s/dns_records", zoneId), nil)
	if err != nil {
		return nil, err
	}
	addParam(req, "name", name)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	payload := new(CloudFlareMessage)
	err = json.NewDecoder(resp.Body).Decode(&payload)
	if err != nil {
		return nil, err
	}
	if !payload.Success {
		return nil, errors.New(fmt.Sprintf("%v", payload.Errors))
	}

	element, ok := payload.Result.([]interface{})
	if !ok {
		return nil, errors.New("result is not an array")
	}

	if len(element) != 1 {
		return nil, errors.New("result has more than one element")
	}

	item, ok := element[0].(map[string]interface{})
	if !ok {
		return nil, errors.New("result[0] is not a map")
	}

	return item, nil
}

func updateRecord(zoneId string, recordId string, name string, ip string) error {
	payload := struct {
		Type    string `json:"type"`
		Name    string `json:"name"`
		Content string `json:"content"`
		Ttl     int    `json:"ttl"`
		Proxied bool   `json:"proxied"`
	}{
		"A",
		name,
		ip,
		120,
		false,
	}

	buffer := bytes.NewBuffer(nil)
	json.NewEncoder(buffer).Encode(payload)
	req, err := newRequest("PUT", fmt.Sprintf("client/v4/zones/%s/dns_records/%s", zoneId, recordId), buffer)
	if err != nil {
		return err
	}

	_, err = http.DefaultClient.Do(req)
	if err != nil {
		return nil
	}

	return nil
}

func getAddr(ifname string) string {
	iface, err := net.InterfaceByName(ifname)
	if err != nil {
		log.Printf("error obtaining interface %v\n", ifname)
		panic(err)
	}
	addrs, err := iface.Addrs()
	if err != nil {
		log.Printf("error obtaining address for interface %v\n", ifname)
		panic(err)
	}
	for _, a := range addrs {
		// skip IPv6 addresses
		if !strings.Contains(a.String(), ":") {
			ip, _, err := net.ParseCIDR(a.String())
			if err != nil {
				panic(err)
			}
			return ip.String()
		}
	}
	return ""
}

func readConfig(filename string) error {

	var config map[string]string
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	d := json.NewDecoder(f)
	err = d.Decode(&config)
	if err != nil {
		return err
	}

	v, ok := config["interface-name"]
	if !ok {
		return errors.New("Missing interface name (interface-name)")
	}
	params.interfaceName = v

	v, ok = config["auth-email"]
	if !ok {
		return errors.New("Missing authentication e-mail (auth-email)")
	}
	params.authEmail = v

	v, ok = config["auth-key"]
	if !ok {
		return errors.New("Missing authentication key (auth-key)")
	}
	params.authKey = v

	v, ok = config["domain"]
	if !ok {
		return errors.New("Missing domain (domain)")
	}
	params.domain = v

	v, ok = config["hostname"]
	if !ok {
		return errors.New("Missing hostname (hostname)")
	}
	params.hostname = v

	v, ok = config["refresh"]
	if ok {
		r, err := strconv.Atoi(v)
		if err == nil {
			params.refresh = r
		}
	}
	return nil
}

func updateIpAddress(addr string) error {
	zoneId, err := getZoneID(params.domain)
	if err != nil {
		return err
	}
	rec, err := getRecord(zoneId, params.hostname)
	if err != nil {
		return err
	}

	if rec["content"] == addr {
		log.Debug("Address is already OK")
	} else {
		log.Infof("Address is %v, updating address with %v", rec["content"], addr)
		err = updateRecord(zoneId, rec["id"].(string), params.hostname, addr)
		if err != nil {
			return err
		}
	}
	return nil
}

func main() {
	var debug = flag.Bool("debug", false, "set debug mode")
	var configFile = flag.String("config file", "config.json", "specify config file")
	flag.Parse()
	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	baseDir := os.Getenv("SNAP_DATA")
	filename := path.Join(baseDir, *configFile)
	log.Debugf("Config file: %v", filename)
	params.refresh = 5

	for {
		err := readConfig(filename)
		if err != nil {
			log.Infof("Invalid configuration: %v", err)
			time.Sleep(5 * time.Second)
			continue
		}

		addr := getAddr(params.interfaceName)
		if addr == "" {
			log.Error("Can't find my IP address")
			time.Sleep(1 * time.Minute)
			continue
		}

		log.Debugf("my IP address: %v", addr)
		updateIpAddress(addr)
		time.Sleep(time.Duration(params.refresh) * time.Minute)
	}
}
