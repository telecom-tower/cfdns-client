// Copyright 2017 Jacques Supcik, HEIA-FR
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//
// Telecom Tower DynDns (cloudflare) client - Configurator
//

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"os"
	"path"
	"sort"
)

var config map[string]string

func readConfig(fileName string) {
	log.Debugf("Reading file %v", fileName)
	f, err := os.Open(fileName)
	if err != nil {
		log.Error(err)
		config = make(map[string]string)
		return
	}
	defer f.Close()
	d := json.NewDecoder(f)
	err = d.Decode(&config)
	if err != nil {
		log.Error(err)
		config = make(map[string]string)
		return
	}
	log.Debug("done")
}

func writeConfig(fileName string) {
	log.Debugf("Creating file %v", fileName)
	f, err := os.Create(fileName)
	if err != nil {
		log.Errorf("Unable to write file %v", fileName)
		os.Exit(1)
	}
	defer f.Close()
	log.Debugln("Writing config")
	e := json.NewEncoder(f)
	err = e.Encode(config)
	if err != nil {
		log.Errorf("Unable to encode config")
		os.Exit(2)
	}
	log.Debug("done")

}

func showConfig() {
	var keys []string
	for k := range config {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		fmt.Printf("%v -> %v\n", k, config[k])
	}
}

func showValue(key string) {
	value, ok := config[key]
	if ok {
		fmt.Printf("%v -> %v\n", key, value)
	} else {
		fmt.Printf("Key %v is not yet defined\n", key)
	}
}

func setValue(key string, value string) {
	config[key] = value
}

func main() {
	var debug = flag.Bool("debug", false, "set debug mode")
	var configFile = flag.String("config file", "config.json", "specify config file")
	flag.Parse()
	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	baseDir := os.Getenv("SNAP_DATA")
	filename := path.Join(baseDir, *configFile)
	log.Debugf("Config file: %v\n", filename)

	readConfig(filename)
	switch flag.NArg() {
	case 0:
		showConfig()
	case 1:
		showValue(flag.Arg(0))
	case 2:
		setValue(flag.Arg(0), flag.Arg(1))
		writeConfig(filename)
	default:
		fmt.Errorf("I don't know what to do with %v arguments", flag.NArg())
	}
}
